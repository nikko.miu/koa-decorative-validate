import joi from '@hapi/joi';

import { Validate, IValidateSchema } from '.';

describe('Validate', () => {
  const createController = (...params: Parameters<typeof Validate>) => class TestController {
    // @ts-ignore
    @Validate(...params)
    testing() { }
  };

  it('validates param for request', () => {
    // Arrange
    const mockNext = jest.fn();
    const validation: IValidateSchema = {

    };
    const ctrl = new (createController(validation))();

    // Act
    ctrl.testing();
  });
});
