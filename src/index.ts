import { Context } from 'koa';
import joi from '@hapi/joi';
import { Pre } from 'koa-decorative';

export interface IValidateSchema {
  headers?: joi.Schema;
  params?: joi.Schema;
  query?: joi.Schema;
  body?: joi.Schema;
}

const sendValidationError = (ctx: Context, errors: string[]) => {
  const status = 'request validation failed';

  ctx.status = 400;
  ctx.body = { status, errors };
};

export interface IValidationOptions extends joi.AsyncValidationOptions {
  stripSchemaless?: boolean;
  sendValidationError?: typeof sendValidationError;
}

const validateOne = async (schema: joi.Schema | undefined, value: any, options: IValidationOptions) => new Promise((resolve, reject) => {
  if (!schema) {
    return resolve(options.stripSchemaless ? null : value);
  }

  schema.validateAsync(value, options)
    .then(result => resolve(options.stripUnknown ? result : value))
    .catch(reject);
});

/* tslint:disable:variable-name */
export const Validate = (schema: IValidateSchema, options?: IValidationOptions) => Pre(async (ctx: Context, next: () => Promise<any>) => {
  const opts: IValidationOptions = {
    sendValidationError,
    abortEarly: false,
    stripUnknown: true,
    stripSchemaless: true,
    ...options,
  };

  try {
    const promises: Promise<any>[] = [
      validateOne(schema.body, (ctx.request as any).body, opts),
      validateOne(schema.headers, ctx.request.headers, opts),
      validateOne(schema.params, ctx.params, opts),
      validateOne(schema.query, ctx.query, opts),
    ];

    const [body, headers, params, query] = await Promise.all(promises);

    (ctx.request as any).body = body;
    ctx.request.headers = headers;
    ctx.params = params;
    ctx.request.query = query;
  } catch (err) {
    if (!opts.sendValidationError) { return; }

    return opts.sendValidationError(ctx, (err as joi.ValidationError).details.map(e => e.message));
  }

  return next();
});
